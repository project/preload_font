CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module preloads the fonts to avoid [flash of unstyled text (FOUT)](https://web.dev/codelab-preload-web-fonts/) issue.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/preload_font

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/preload_font


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the preload font module as you would normally install a contributed
   Drupal module. Visit [Installing Modules](https://www.drupal.org/node/1897420) for further
   information.

The configuration page is at admin/config/user-interface/preload-font,
 where you can configure the font paths.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > User interface >
       Preload font settings.
    3. Add the font paths you want to preload in <head> tag.
    4. Save configuration.


MAINTAINERS
-----------

 * Jaykumar Pawar (jaykumar95) - https://www.drupal.org/u/jaykumar95

Supporting organizations:

 * ByteParity - https://www.drupal.org/byteparity
