<?php

namespace Drupal\preload_font\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Polyfill\Php80\Php80;

/**
 * Configure Preload font settings for this site.
 */
class PreloadFontSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'preload_font_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['preload_font.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('preload_font.settings');
    $form['fontPaths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Font paths'),
      '#default_value' => $config->get('fontPaths'),
      '#placeholder' => $this->t("/relative/path/to/font.woff\nhttps://fonts.googleapis.com/css?family=Roboto:400,700&display=swap"),
      '#required' => FALSE,
      '#description' => $this->t('One URL per line, You can use absolute path and relative path with leading slash.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $paths = array_unique(array_filter(explode(PHP_EOL, $values['fontPaths'] ?? '')));

    // Validate the URLs.
    foreach ($paths as $path) {
      $path = trim($path);
      if (!UrlHelper::isValid($path, Php80::str_starts_with($path, 'http')) || (!Php80::str_starts_with($path, '/')) && (!Php80::str_starts_with($path, 'http'))) {
        $form_state->setErrorByName('fontPaths', $this->t("@url is not valid or should start with a leading slash or http.", ['@url' => $path]));
      }
      elseif (Php80::str_ends_with($path, '/')) {
        $form_state->setErrorByName('fontPaths', $this->t("@url is not valid or should not end with trailing slash.", ['@url' => $path]));
      }
      elseif (Php80::str_contains($path, 'fonts.googleapis.com') && !Php80::str_contains($path, 'display=swap')) {
        $form_state->setErrorByName('fontPaths',
        $this->t('Add the &display=swap parameter to the end of your Google Fonts URL: @url to avoid <a href="@token" target="_blank">FOIT issue</a>.',
        [
          '@url' => $path,
          '@token' => 'https://web.dev/font-display/#preload-web-fonts',
        ]
        ));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $paths = array_unique(array_filter(explode(PHP_EOL, $values['fontPaths'] ?? '')));
    $paths = implode(PHP_EOL, $paths);
    $this->config('preload_font.settings')
      ->set('fontPaths', $paths)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return ['config:preload_font.settings'];
  }

}
